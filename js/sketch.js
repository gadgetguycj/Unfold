//const Matter = require("./matter.js");
let Engine = Matter.Engine,

World = Matter.World;
Bodies = Matter.Bodies;

let engine;
let world;
let boxes = [];
let ground;

function setup() {
    createCanvas(screen.width, 400);
    engine = Engine.create();
    world = engine.world;
    Engine.run(engine);

    ground = new Boundary(screen.width/2, height, screen.width, 100);
    World.add(world, ground);

}
function mouseDragged() {
    boxes.push(new Box(mouseX, mouseY, 20, 20));
}

function draw() {
    background(51);
    for (let o of boxes) {
        o.show();
    }
    ground.show();

}